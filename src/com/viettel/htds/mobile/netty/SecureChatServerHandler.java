/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.viettel.htds.mobile.netty;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.viettel.htds.mobile.Const;
import com.viettel.htds.mobile.httpclient.BaseHttpClient;
import com.viettel.htds.mobile.model.RestJson;
import com.viettel.htds.mobile.model.UserToken;
import com.viettel.htds.mobile.utils.JsonUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import java.util.HashMap;
import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import viettel.passport.client.AppToken;
import viettel.passport.client.ObjectToken;
import viettel.passport.client.RoleToken;
import viettel.passport.client.VSAValidate;

/**
 * Handles a server-side channel.
 */
public class SecureChatServerHandler extends SimpleChannelInboundHandler<String> {

    public static final ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    private static final HashMap<String, UserToken> cache = new HashMap<String, UserToken>();

    @Override
    public void channelActive(final ChannelHandlerContext ctx) {
        try {
//            ctx.channel().toString();
            // Once session is secured, send a greeting and register the channel to the global channel
            // list so the channel received the messages from others.
//        ctx.pipeline().get(SslHandler.class).handshakeFuture().addListener(
//                new GenericFutureListener<Future<Channel>>() {
//                    @Override
//                    public void operationComplete(Future<Channel> future) throws Exception {
//                        ctx.writeAndFlush(
//                                "Welcome to " + InetAddress.getLocalHost().getHostName() + " secure chat service!\n");
//                        ctx.writeAndFlush(
//                                "Your session is protected by " +
//                                        ctx.pipeline().get(SslHandler.class).engine().getSession().getCipherSuite() +
//                                        " cipher suite.\n");
//
//                        channels.add(ctx.channel());
//                    }
//        });
//            String tmp = ctx.channel().toString();
//            System.out.println(tmp);
//            String id = tmp.substring(tmp.indexOf(":") + 1, tmp.indexOf(",")).trim();

//            ctx.writeAndFlush(id);
//            ctx.writeAndFlush(
//                    "Your session is protected by " +
//                            ctx.pipeline().get(SslHandler.class).engine().getSession().getCipherSuite() +
//                            " cipher suite.\n");
            channels.add(ctx.channel());
            String channelId = getChannelId(ctx);
            String tmp = JsonUtil.restJson();
            System.out.println("channelId:" + channelId + " TEMPLATE: " + tmp);
        } catch (Exception ex) {
            Logger.getLogger(SecureChatServerHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        System.out.println("exceptionCaught: " + cause.toString());
        ctx.close();
    }

    private String getChannelId(ChannelHandlerContext ctx) {
        String s = ctx.channel().toString();
        String channelId = s.substring(s.indexOf(":") + 1, s.indexOf(",")).trim();
        return channelId;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) {
        // Send the received message to all channels but the current one.
//        for (Channel c: channels) {
//            if (c != ctx.channel()) {
//                c.writeAndFlush("[" + ctx.channel().remoteAddress() + "] " + msg + '\n');
//            } else {
//                c.writeAndFlush("[you] " + msg + '\n');
//            }
//        }
        try {
            if ("bye".equals(msg.toLowerCase())) {
                ctx.close();
                // Close the connection if the client has sent 'bye'.
            } else {
                String channelId = getChannelId(ctx);
                System.out.println("REVEIVE From channelId " + channelId + ":\n" + msg);
                JSONObject msgJson = null;

                try {
                    msgJson = new JSONObject(msg);
                } catch (JSONException e) {
                    ctx.writeAndFlush("BYE\n");
                    ctx.close();
                    System.out.println("Exception: " + e.toString());
                    return;
                }

                String clientRequestCode = msgJson.getString("code");
                System.out.println("code: " + clientRequestCode);
                Gson gson = new Gson();
                if (clientRequestCode != null && !clientRequestCode.isEmpty()) {
                    if (Const.REST_CODE.equalsIgnoreCase(clientRequestCode)) {
                        JSONObject restJson = msgJson.getJSONObject("rest");
                        RestJson rest = null;
                        try {
                            rest = gson.fromJson(restJson.toString(), RestJson.class);
                        } catch (JsonSyntaxException e) {
                            ctx.writeAndFlush("BYE\n");
                            ctx.close();
                            System.out.println("Exception: " + e.toString());
                            return;
                        }

//                        System.out.println(rest);
                        if (Const.USER_LOGIN.equalsIgnoreCase(rest.getPath())) {
                            vsaLogin(ctx, rest, channelId, restJson);
                        } else {
                            String username = msgJson.getString("username");
                            String session = msgJson.getString("session");
                            UserToken currentLogin = new UserToken(username, session, channelId, System.currentTimeMillis());
                            int isSession = checkSession(currentLogin);
                            if (isSession == 0) {
                                String httpResult = BaseHttpClient.request(Const.SERVER_PATH + rest.getPath(), rest.getParams(), rest.getMethod(), restJson);
                                
                                if(ctx != null) {
                                    System.out.println("channelId: " + channelId + " isRemoved " + ctx.isRemoved());
                                } else {
                                    System.out.println("channelId: " + channelId + " is NULL ");
                                }
                                System.out.println(httpResult);
                                ctx.writeAndFlush(httpResult + "\n");
                            } else {
                                JSONObject json = new JSONObject();
                                json.put("code", Const.ERROR_CODE.SESSION_TIMEOUT.ordinal());
                                if (isSession == 1) {
                                    json.put("message", "Tài khoản đã đăng nhập ở thiết bị khác");
                                } else {
                                    json.put("message", "Phiên đăng nhập hết hạn");
                                }
                                json.put("data", new JSONArray());
                                ctx.writeAndFlush(json.toString() + "\n");
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            try {
                JSONObject json = new JSONObject();
                json.put("code", Const.ERROR_CODE.EXCEPTION.ordinal());
                json.put("message", e.getMessage());
                json.put("data", "[]");
                ctx.writeAndFlush(json.toString() + "\n");

                System.out.println("Exception: " + e.toString());
            } catch (JSONException ex) {
                System.out.println("JSONException: " + ex.toString());
            }
        }

    }

    private int checkSession(UserToken user) {
        int isSession = 0;
        user.setUsername(user.getUsername().toLowerCase());
        System.out.println("checkSession current: " + user.toString());
        UserToken obj = cache.get(user.getUsername().toLowerCase());
        if (obj != null) {
            System.out.println("cache session " + obj.toString() + " cache.size " + cache.size());
            if (obj.equals(user)) {
                if (!obj.getSession().equalsIgnoreCase(user.getSession())) {
                    isSession = 1;
                } else if (obj.getUpdateTime() + Const.SESSION_TIME_OUT < user.getUpdateTime()) {
                    isSession = 2;
                } else {
                    isSession = 0;
                    cache.put(user.getUsername().toLowerCase(), user);
                }
            }
        }
        return isSession;
    }

    private void vsaLogin(ChannelHandlerContext ctx, RestJson rest, String channelId, JSONObject restJson) throws Exception {
        String username = rest.getParams().get(0).getValue();
        String password = rest.getParams().get(1).getValue();
        System.out.println("vsaLogin " + username);

        VSAValidate vsa = new VSAValidate();
        vsa.setCasValidateUrl("https://10.58.71.137:8566/passportv3/passportWS?wsdl");
        vsa.setDomainCode("HTDS_Portal");
        vsa.setUser(username);
        vsa.setPassword(password);
        vsa.validate();
        if (vsa.isAuthenticationSuccesful()) {
            JSONArray role = parseRoleVSA(vsa.getUserToken().getRolesList());
            JSONArray app = parseAppVSA(vsa.getAllApp());
            JSONArray menu = new JSONArray(vsa.getUserToken().getObjectTokens());
            int vsaUserType = parseMenuVSA(vsa.getUserToken().getObjectTokens(), vsa.getUserToken().getRolesList());

            String httpResult = BaseHttpClient.request(Const.SERVER_PATH + rest.getPath(), rest.getParams(), rest.getMethod(), restJson);
            System.out.println(httpResult);
            JSONObject loginJson = new JSONObject(httpResult);
            int code = loginJson.getInt("code");
            if (code == 0) {
                UserToken u = new UserToken(username.toLowerCase(), UserToken.nextSessionId(), channelId, System.currentTimeMillis());
                cache.put(u.getUsername().toLowerCase(), u);

                System.out.println("Login Success: " + username + " " + u.getSession());

                JSONArray data = loginJson.getJSONArray("data");
                JSONObject dataObject = data.getJSONObject(0);

                dataObject.put("role", role);
                dataObject.put("app", app);
                dataObject.put("menu", menu);
                dataObject.put("partnerUserType", vsaUserType);
                dataObject.put("channelId", channelId);
                dataObject.put("ip", ctx.channel().remoteAddress().toString());
                dataObject.put("session", u.getSession());

                data.remove(0);
                data.put(dataObject);

                loginJson.remove("data");
                loginJson.put("data", data);
            }
            ctx.writeAndFlush(loginJson.toString() + "\n");
        } else {
            JSONObject json = new JSONObject();
            json.put("code", Const.ERROR_CODE.LOGIN_FAIL.ordinal());
            json.put("message", "Tài khoản hoặc mật khẩu không đúng");
            json.put("data", "[]");
            ctx.writeAndFlush(json.toString() + "\n");
        }
    }

    private JSONArray parseRoleVSA(List<RoleToken> lst) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        for (RoleToken roleToken : lst) {
            JSONObject json = new JSONObject();
            json.put("roleCode", roleToken.getRoleCode());
            json.put("roleName", roleToken.getRoleName());
            json.put("description", roleToken.getDescription());
            jSONArray.put(json);
        }
        return jSONArray;
    }

    private JSONArray parseAppVSA(List<AppToken> lst) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        for (AppToken appToken : lst) {
            JSONObject json = new JSONObject();
            json.put("appCode", appToken.getAppCode());
            json.put("appName", appToken.getAppName());
            jSONArray.put(json);
        }
        return jSONArray;
    }

    private int parseMenuVSA(List<ObjectToken> lst, List<RoleToken> rolesList) throws JSONException {
        int manager = 0;
        int staff = 0;

        for (RoleToken role : rolesList) {
            if (role.getRoleCode().equalsIgnoreCase("HTDS_PORTAL_GIANGDT")) {
                return 4;
            }
        }

        for (ObjectToken objectToken : lst) {
            if (objectToken.getObjectCode().equalsIgnoreCase("HTDS_PORTAL_IC_REPORT4")) {
                manager = 1;
            }
            if (objectToken.getObjectCode().equalsIgnoreCase("HTDS_PORTAL_IC_REPORT_APPROVE_STAFF")) {
                staff = 2;
            }
        }

        return staff + manager;
    }
}
