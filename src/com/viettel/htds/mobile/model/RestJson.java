/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.htds.mobile.model;

import java.util.List;

/**
 *
 * @author Aloha
 */
public class RestJson {
    private String path;
    private String method;
    private List<Param> params;
    private String body;

    public RestJson() {
    }

    public RestJson(String path, String method, List<Param> params) {
        this.path = path;
        this.method = method;
        this.params = params;
    }
    
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public List<Param> getParams() {
        return params;
    }

    public void setParams(List<Param> params) {
        this.params = params;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "RestJson{" + "path=" + path + ", method=" + method + ", params=" + params + '}';
    }

    
}
