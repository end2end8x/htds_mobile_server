/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.htds.mobile.model;

/**
 *
 * @author Aloha
 */
public class ClientRest {
    private String code;
    private String username;
    private String session;
    private RestJson rest;

    public ClientRest() {
    }

    public ClientRest(String code, String username, String session, RestJson rest) {
        this.code = code;
        this.username = username;
        this.session = session;
        this.rest = rest;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public RestJson getRest() {
        return rest;
    }

    public void setRest(RestJson rest) {
        this.rest = rest;
    }

    @Override
    public String toString() {
        return "ClientRest{" + "code=" + code + ", username=" + username + ", session=" + session + ", rest=" + rest + '}';
    }
}
