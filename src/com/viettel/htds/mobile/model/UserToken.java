/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.htds.mobile.model;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Objects;

/**
 *
 * @author Aloha
 */
public class UserToken {
    private String username;
    private String session;
    private String channelId;
    private long updateTime;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
    
    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public UserToken() {
    }

    public UserToken(String username, String session, String channelId, long updateTime) {
        this.username = username;
        this.session = session;
        this.channelId = channelId;
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof UserToken) {
            UserToken pp = (UserToken) obj;
            return (pp.username.toLowerCase().equalsIgnoreCase(this.username.toLowerCase()));
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.username);
        return hash;
    }

    @Override
    public String toString() {
        return "UserToken{" + "username=" + username + ", session=" + session + ", channelId=" + channelId + ", updateTime=" + updateTime + '}';
    }
    
    public static String nextSessionId() {
        SecureRandom random = new SecureRandom();
        return new BigInteger(130, random).toString(32);
    }
    
    
}
