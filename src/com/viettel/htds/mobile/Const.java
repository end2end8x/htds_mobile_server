/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.htds.mobile;

/**
 *
 * @author Aloha
 */
public interface Const {
    String SERVER_PATH = "http://10.58.71.137:8756/HTDS/mobile/";
//    String SERVER_PATH = "http://10.61.9.61:8080/HTDS/mobile/";
    String REST_CODE = "rest";
    String GET = "get";
    String POST = "post";
    String USER_LOGIN = "login";
    String DOWNLOAD_REPORT = "downloadReport";
    
    long SESSION_TIME_OUT = 3600000;
    
    int HTTP_CONNECT_TIMEOUT = 20000;
    int HTTP_READ_TIMEOUT = 70000;
    
    public enum ERROR_CODE {
        SUCCESS,
        EXCEPTION,
        SESSION_TIMEOUT,
        INPUT_INVALID,
        LOGIN_FAIL,
        FILE_NOT_FOUND,
        FILE_DUPLICATE,
        SIMCA,
        NO_DATA,
        DATABASE,
        NETWORK,
        CONNECTION,
        TIME_OUT
    }
}
