/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.htds.mobile.httpclient;

import com.viettel.htds.mobile.Const;
import com.viettel.htds.mobile.model.Param;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.List;
import org.json.JSONObject;

/**
 *
 * @author Aloha
 */
public class BaseHttpClient {

    public static String request(String path, List<Param> list, String method, JSONObject json) throws Exception {
        String data = json.toString();
        
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(path);
//        if (list != null && !list.isEmpty()) {
//            urlBuilder.append("?");
//            for (Param param : list) {
//                urlBuilder.append(param.getName());
//                urlBuilder.append("=");
//                urlBuilder.append(URLEncoder.encode(param.getValue(), "utf-8"));
//                urlBuilder.append("&");
//            }
//            urlBuilder.deleteCharAt(urlBuilder.length() - 1);
//        }
        
        System.out.println("BaseHttpClient request " + urlBuilder.toString() + " method " + method + " data " + data);

        String responseString = "";
        StringBuilder outputString = new StringBuilder();
        URL url = new URL(urlBuilder.toString());
        URLConnection connection = url.openConnection();
        connection.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
        connection.setRequestProperty("Accept-Charset", "utf-8");
        connection.setConnectTimeout(Const.HTTP_CONNECT_TIMEOUT);
        connection.setReadTimeout(Const.HTTP_READ_TIMEOUT);
        
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        byte[] buffer = new byte[data.length()];
        buffer = data.getBytes();
        bout.write(buffer);
        byte[] b = bout.toByteArray();

        HttpURLConnection httpConn = (HttpURLConnection) connection;
        // Set the appropriate HTTP parameters.
        httpConn.setRequestProperty("Content-Length", String.valueOf(b.length));
        httpConn.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
        httpConn.setRequestMethod(method.toUpperCase());
        httpConn.setDoOutput(true);
        httpConn.setDoInput(true);
        OutputStream out = httpConn.getOutputStream();
        // Write the content of the request to the outputstream of the HTTP
        // Connection.
        out.write(b);
        out.close();
        // Ready with sending the request.
        // Read the response.
        InputStreamReader isr = new InputStreamReader(
                httpConn.getInputStream());
        BufferedReader in = new BufferedReader(isr);

        while ((responseString = in.readLine()) != null) {
            outputString.append(responseString);
        }
        
        httpConn.disconnect();
        return outputString.toString();
    }

}
